## webpage_authentication_cookie_session

Webpage authentication using a client-side session, inside a cookie.  

Key-points:  
 - cookies are handled by the browser (and are automatically sent with each request)  
 - client-side session; server has no session info and only checks if cookie is valid (session is easier to share but difficult to control, server needs less resources)  
 - cookie contains full session info (can get "big" - 4KB; can create some overhead since cookies are sent in every request)